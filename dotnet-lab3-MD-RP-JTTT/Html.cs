﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Net;
using System.Windows.Forms;

namespace dotnet_lab3_MD_RP_JTTT
{

    [Serializable]
    public class Html
    {
        public int HtmlID { get; set; }

        private string _url;
        private string _phrase;

        public Html(string url, string phrase)
        {
            this._url = url;
            this._phrase = phrase;
        }
        public Html()
        { }

        // właściwośći
        public string Url
        {
            get
            {
                return _url;
            }
            set
            {
                _url = value;
            }
        }

        public string Phrase
        {
            get
            {
                return _phrase;
            }
            set
            {
                _phrase = value;
            }
        }
        public override string ToString()
        {
            return string.Format("_url={0}, _phrase={1}", _url, _phrase);
            //St Id={0}, Name={1}, Adres={2}, Ur.={3}, Grupa={4}"
        }


        /// <summary>
        /// Prosta metoda, która zwraca zawartość HTML podanej strony www
        /// </summary>
        public string GetPageHtml()
        {
            using (var wc = new WebClient())
            {
                try { 
                // Ustawiamy prawidłowe kodowanie dla tej strony
                wc.Encoding = Encoding.UTF8;
                // Dekodujemy HTML do czytelnych dla wszystkich znaków 
                var html = System.Net.WebUtility.HtmlDecode(wc.DownloadString(_url));

                return html;
                }
                catch
                {
                    return null;
                }
            }
        }



        /// <summary>
        /// Równie prosta metoda, która wypisuje na konsole wartości atrybutów src oraz alt taga IMG
        /// znajdujących się na podanej stronie www
        /// </summary>
        public bool DownloadPageImage()
        {
            // Tworzymy obiekt klasy HtmlDocument zdefiniowanej w namespace HtmlAgilityPack
            // Uwaga - w referencjach projektu musi się znajdować ta biblioteka
            // Przy użyciu nuget-a pojawi się tam automatycznie
            var doc = new HtmlAgilityPack.HtmlDocument();

            // Używamy naszej metody do pobrania zawartości strony
            var pageHtml = GetPageHtml();
            if (pageHtml == null)
            {
                MessageBox.Show("Wprowadzono nieprawidlowy URL");
                System.Environment.Exit(1);
            }
            // Ładujemy zawartość strony html do struktury documentu (obiektu klasy HtmlDocument)
            doc.LoadHtml(pageHtml);

            // Metoda Descendants pozwala wybrać zestaw node'ów o określonej nazwie
            var nodes = doc.DocumentNode.Descendants("img");

            // Iterujemy po wszystkich znalezionych node'ach
            foreach (var node in nodes)
            {
                try
                {
                    if (node.GetAttributeValue("alt", "").Contains(_phrase))
                    {
                        using (var client = new WebClient())
                        {
                            client.Credentials = new NetworkCredential("UserName", "Password");
                            client.DownloadFile(node.GetAttributeValue("src", ""), _phrase + ".png");

                        }

                        return true;
                    }
                    //MessageBox.Show("Znalazlem obrazek z podpisem " + _phrase);
                }
                catch
                {
                    MessageBox.Show("Błąd serwera !");
                }          
            }

            return false;
        }

    }
}
