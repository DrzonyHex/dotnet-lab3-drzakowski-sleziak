﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;
using System.Net;


namespace dotnet_lab3_MD_RP_JTTT
{
    [Serializable]
    public class EmailClass
    {
        public int EmailClassID { get; set; }

        private string _mailto;
        private string _subject;
        public string _body { get; set; }
        public string _attachment { get; set; }

        public EmailClass()
        { }

        public EmailClass(string mailto, string attachment, string subject, string body )
        {
            this._mailto = mailto;
            this._subject = subject;
            this._body = body;
            this._attachment = attachment;

        }

        public override string ToString()
        {
            return string.Format("_mailto={0}, _subject={1}, _body={2}, _attachment={3}", _mailto, _subject, _body, _attachment);

        }

        public string MailTo
        {
            get
            {
                return _mailto;
            }
            set
            {
                _mailto = value;
            }
        }

        public string Subject
        {
            get
            {
                return _subject;
            }
            set
            {
                _subject = value;
            }
        }


  

        public void SendEmail()
        {
                try
                {

                    var fromAddress = new MailAddress("testdrzonhex@gmail.com", "MD & RP");
                    var toAddress = new MailAddress(_mailto, "MD & RP ");
                    string fromPassword = "DotNetLab3";
                    string subject = _subject;
                    string body = _body;


                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                    };
            using (var message = new MailMessage(fromAddress, toAddress))
            {
                message.Subject = subject;
                message.Body = body;
                
                //dodwanie zalacznika
                message.Attachments.Add(new System.Net.Mail.Attachment(_attachment));

                {
                    smtp.Send(message);
                }
            }
                }//nawias -> try
                catch
                {
                   MessageBox.Show("Podany ciąg nie ma formy wymaganej dla adresu e - mail.");
                }
        }

    }

}



      