﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dotnet_lab3_MD_RP_JTTT
{
    [Serializable]
    public class Task
    {
        public int TaskID { get; set; }
        public string _taskName { get; set; }
        private LogClass _log { get; set; }

        public virtual Html _condition { get; set; }
        public virtual EmailClass _action { get; set; }

        public Task()
        { }

        public Task(string taskName, string url, string phrase, string mailto, string subject, string body)
        {
            string attachment = phrase + ".png";
            _condition = new Html(url, phrase);
            _action = new EmailClass(mailto, attachment, subject, body);
            _taskName = taskName;
            _log = new LogClass(url, phrase, mailto);

        }

        public void DoTask()
        {
            if (_condition.DownloadPageImage())
            {
                _action.SendEmail();
                //_log.CreateLogImage();
                MessageBox.Show("Znaleziono obrazek! ");
            }
            else
            {
                //_log.CreateLogNotImage();
                MessageBox.Show("Nie znaleziono obrazka");
            }
        }


        public string ToString2()
        {
            return "TaskName: " + _taskName + " | Url: " + _condition.Url + " || Phrase: " + _condition.Phrase + " || MailTo: " + _action.MailTo + " || Subject: " + _action.Subject + "||";
        }

        public override string ToString()
        {
            return string.Format("Task Name={0}, Action={1}, Condition={2}", _taskName, _action.ToString(), _condition.ToString() );
        }

    }
}
