﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ComponentModel;

namespace dotnet_lab3_MD_RP_JTTT
{
    class SerializeClass
    {


        public SerializeClass() { }

        public void Serialize(BindingList<Task> tasks)
        {
            FileStream fs = new FileStream("SerializeFile.dat", FileMode.Create);
            BinaryFormatter formatter = new BinaryFormatter();

            try
            {
                formatter.Serialize(fs, tasks);
            }
            catch (SerializationException e)
            {
                MessageBox.Show(e.Message);
                throw;
            }
            finally
            {
                fs.Close();
            }

        }

        public BindingList<Task> Deserialize()
        {
            BindingList<Task> task;

            FileStream fs = new FileStream("SerializeFile.dat", FileMode.Open);

            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                return task = (BindingList<Task>)formatter.Deserialize(fs);
            }
            catch (SerializationException e)
            {
                MessageBox.Show(e.Message);
                throw;
            }
            finally
            {
                fs.Close();
            }
        }


    }
}
