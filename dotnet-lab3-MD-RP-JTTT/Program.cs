﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using System.Data.Entity.Migrations;


namespace dotnet_lab3_MD_RP_JTTT
{
    public class TaskContext : DbContext
    {
        public TaskContext() : base("NowaDb4")
        {
            Database.SetInitializer<TaskContext>(new CreateDatabaseIfNotExists<TaskContext>());
            //Database.SetInitializer(new StudiaDbInitializer());
        }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Html> Htmls { get; set; }
        public DbSet<EmailClass> EmailClasss { get; set; }

        
    }
  

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
