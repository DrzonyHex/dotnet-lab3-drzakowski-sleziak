﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;
using System.Net;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using Newtonsoft.Json;

namespace dotnet_lab3_MD_RP_JTTT
{
    public partial class Form1 : Form
    {
        private string url; //url strony na ktorej bedzie szukana fraza
        private string phrase; //poszukiwana fraza obrazka
        private string email; //docelowy email, na ktory bedzie wyslana wiadomosc
        private string taskName;
        const string APPID = "cc68a686b603538e13a9e1477dc3821d";

        BindingList<Task> listOfParts;

        private void InitializeListOfParts()
        {
            listOfParts = new BindingList<Task>();
            listOfParts.AllowNew = true;
            listOfParts.AllowRemove = true;
            listOfParts.RaiseListChangedEvents = true;
            listOfParts.AllowEdit = true;
        }

        public void ConnectToDb()
        {

        }

        public Form1()
        {
            InitializeComponent();
            InitializeListOfParts();
        }

        private void textBoxURL_TextChanged(object sender, EventArgs e)
        {
            url = textBoxURL.Text;
        }

        private void textBoxPhrase_TextChanged(object sender, EventArgs e)
        {
            phrase = textBoxPhrase.Text;
        }

        private void textBoxEmail_TextChanged(object sender, EventArgs e)
        {
            email = textBoxEmail.Text;
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            foreach (Task a in listOfParts)
            {
                a.DoTask();
            }
        }

        private void buttonAddTask_Click(object sender, EventArgs e)
        {
            if (url == null || phrase == null || email == null || taskName == null)
                MessageBox.Show("Uzupelnij wszystkie dane!");
            else
            {
                Task tmp = new Task(taskName, url, phrase, email, "obrazek na dzis", "Obrazek ze strony" + url);
                listOfParts.Add(tmp);
                listBox.Items.Add(tmp.ToString());
                using (var ctx = new TaskContext())
                {
                ctx.Tasks.Add(tmp);
                ctx.SaveChanges();
                }
            }
        }

        private void textTaskName_TextChanged(object sender, EventArgs e)
        {
            taskName = textTaskName.Text;
        }

        private void listBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        private void buttonClean_Click(object sender, EventArgs e)
        {
            try
            {
                if (listOfParts.Any())
                {
                   /* using (var ctx = new TaskContext())
                    {

                        //ctx.Children.RemoveRange(parent.Children)
                        int id = 1;
                        var task = new Task { TaskID = id };
                        var html = new Html { HtmlID = id };
                        var emailClass = new EmailClass { EmailClassID = id };
                        ctx.Entry(task).State = EntityState.Deleted;
                        ctx.Entry(html).State = EntityState.Deleted;
                        ctx.Entry(emailClass).State = EntityState.Deleted;

                        ctx.SaveChanges();

                    }*/

                    listOfParts.RemoveAt(listBox.SelectedIndex);
                    listBox.Items.RemoveAt(listBox.SelectedIndex);
                }
                else
                {
                    MessageBox.Show("Brak Tasków!");
                }
            }
            catch
            {
                MessageBox.Show("Nie wybrano tasku do usunięcia");
            }
        }

        private void buttonSerialize_Click(object sender, EventArgs e)
        {
            SerializeClass s = new SerializeClass();
            s.Serialize(listOfParts);
            MessageBox.Show("Serialized");
        }


        private void buttonDeSerialize_Click(object sender, EventArgs e)
        {
            SerializeClass s = new SerializeClass();

            foreach (var tsk in s.Deserialize())
            {
                listOfParts.Add(tsk);
                listBox.Items.Add(tsk.ToString2());
            }

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            using (var ctx = new TaskContext())
            {

                foreach (Task g in ctx.Tasks)
                {
                    listOfParts.Add(g);
                    listBox.Items.Add(g.ToString());    
                }
            }

          
        }

        private void buttonWeather_Click(object sender, EventArgs e)
        {
            try
            {

                string city = textCityWeather.Text;

                using (WebClient web = new WebClient())
                {
                    string url = string.Format("http://api.openweathermap.org/data/2.5/weather?q="+city+",pl&APPID="+APPID);

                    var json = web.DownloadString(url);

                    var result = JsonConvert.DeserializeObject<WeatherClass.RootObject>(json);

                    WeatherClass.RootObject outPut = result;

                    labelCityName.Text = string.Format("{0}", outPut.name);
                    labelCountryName.Text = string.Format("{0}", outPut.sys.country);
                    labelTemp.Text = string.Format("{0} \u00B0 " + "C", (double)outPut.main.temp - 273, 15);
                    labelPressure.Text = string.Format("{0} \u00B0 " + "hPa", (double)outPut.main.pressure );

                    var picture_id = outPut.weather[0].icon;
                    var picture = "http://openweathermap.org/img/w/" + picture_id + ".png";

                    pictureBoxWeather.ImageLocation = picture;
                    pictureBoxWeather.SizeMode = PictureBoxSizeMode.AutoSize;
                    

                }
            }
            catch
            {
                MessageBox.Show("Nie podano nazwy miasta, lub jest ona nieprawidłowa!");
            }


        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }
    }
}




