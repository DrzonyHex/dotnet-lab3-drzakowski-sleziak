﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace dotnet_lab3_MD_RP_JTTT
{
    [Serializable]
    public class LogClass
    {
        private string _url;
        private string _phrase;
        private string _mail;

        DateTime thisDay = DateTime.Now;

        public LogClass(string url, string phrase, string mail)
        {
            this._url = url;
            this._phrase = phrase;
            this._mail = mail;
        }

        public void CreateLogImage()
        {
            using (StreamWriter file = new StreamWriter("log_image.log", true))
            {
                file.WriteLine("\n");
                file.WriteLine(thisDay.ToString());
                file.WriteLine("URL: " + _url);
                file.WriteLine("Szukana fraza: " + _phrase);
                file.WriteLine("Adres e-mail: " + _mail);
                file.WriteLine("Znaleziono obrazek. \n");
            }
        }
        public void CreateLogNotImage()
        {
            using (StreamWriter file = new StreamWriter("log_image.log", true))
            {
                file.WriteLine("\n");
                file.WriteLine(thisDay.ToString());
                file.WriteLine("URL: " + _url);
                file.WriteLine("Szukana fraza + _phrase");
                file.WriteLine("Adres e-mail: " + _mail);
                file.WriteLine("Nie znaleziono obrazka. \n");
            }
        }

    }
}
